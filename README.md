# 美鉴
## 设计目标

拥有完整功能的美妆鉴别平台，可查阅其他用户发布的购物心得以及鉴别官发布的鉴别真假知识、可对帖子进行点赞、评论、收藏、可进行优质商品的购买、可对产品鉴别真假、可开通vip享优惠。

## 功能描述
1. 登录功能，手机号密码进行登录
2. 浏览各大美妆知识，可进行点赞、评论、收藏
3. 可购买各种种类的美妆产品
4. 对帖子进行搜索以及美妆产品搜索、历史记录
5. 可参与抽奖以及观看中奖幸运儿
6. 付费鉴别
7. 换头像以及换昵称，同时评论区也会同步
8. 添加、删除、修改地址
9. 完整的购物车流程/待发货/已收货/已完成

## 使用到策略模式
1. 购买页中的商品列表中运用到了策略模式(针对一组算法，将每一个算法封装到具有共同接口的独立的类中，使得它们可以互换。)，根据数据来决定结构。
代码如下：

```html

    <!-- 底部产品 -->
      <div class="buyItem">
        <ul>
          <li v-for="item in buyItem" :key="item.singleId" >
            <!-- 正常产品 -->
              <div v-if="!item.backgroundUrl" @click="buy(item.commodityId)">
                  <div class="img">
                  <img
                    :src="item.imgUrl"
                    alt=""
                  />
                </div>
                <div class="title">
                  <p>{{item.brandName}} {{item.commodityName}} {{item.standardValue}}</p>
                </div>
                <div class="price">
                  <span class="red"><i>￥</i>{{parseInt(item.price)}}</span>
                  <span class="ka">黑卡</span>
                  <span class="blok"><i>￥{{parseInt(item.blackPrice)}}</i></span>
                </div>
                <div class="num">{{item.payPersons}}人付款</div>
              </div>

            <!-- 广告 -->
            <div v-if="item.backgroundUrl" @click="advert(item.commodityId)">
                 <div class="bg">
                <img
                  :src="item.backgroundUrl"
                  alt=""
                />
              </div>
              <div class="small_img">
                <img
                  :src="item.imgUrl"
                  alt=""
                />
              </div>
             </div>

          </li>
        </ul>
      </div>

```
图片效果：

![](./assets/../src/assets/imgs/18产品列表.png)


2. 帖子评论
   
    因帖子回复的人有两种，作者回复和普通用户回复，所以需要判断数据来写结构
    代码如下：
```html
  <!-- 评论 -->
    <div class="comment">
      <div class="num">共{{ details.comment.length }}条评论</div>
      <!-- 空 -->
      <div class="null" v-if="!details.comment.length">
        <p class="wu">暂无评论</p>
        <p class="end">THE END</p>
      </div>
      <!-- 有评论 -->
      <div class="comm_item" v-if="details.comment.length">
        <ul>
          <li v-for="(item, index) in details.comment" :key="index">
            <div class="comm" @click="one(2,index)">
              <div class="img">
                <img :src="item.headImgUrl" alt="" />
              </div>
              <div class="title">
                <div class="name">{{ item.userName }}</div>
                <div class="all_title">
                  <p class="p">
                    {{ item.content }}
                  </p>
                  <span class="time"
                    >{{
                      String(item.createTime.split(" ")[0]).split("-")[1]
                    }}月{{
                      String(item.createTime.split(" ")[0]).split("-")[2]
                    }}日</span
                  >
                </div>
              </div>
              <span
                class="iconfont icon-aixin"
                v-if="!item.aixin1"
                @click.stop="aixin1(1, index)"
                ><i>{{ item.praise == 0 ? "" : item.praise }}</i></span
              >
              <span
                class="iconfont icon-aixin2"
                v-if="item.aixin1"
                @click.stop="aixin1(2, index)"
                ><i>{{ item.praise }}</i></span
              >
            </div>
            <!-- 回复 -->
            <div v-if="item.items.length">
              <div
                class="reply"
                v-for="(small, index1) in item.items"
                :key="index1"
                @click="two(3,index,index1)"
              >
                <div class="people">
                  <div class="left">
                    <div class="img">
                        <!-- 判断头像 -->
                      <img
                        :src="
                          details.operateUserName == small.answerNickName
                            ? small.answerHeadImgUrl
                            : small.headImgUrl
                        "
                        alt=""
                      />
                    </div>
                    <!-- 判断回复名称与发布名称是否相同 -->
                    <p class="names">
                      {{
                        details.operateUserName == small.answerNickName
                          ? small.answerNickName
                          : small.userName
                      }}
                      <!-- 是作者才需要显示 -->
                      <span
                        v-if="details.operateUserName == small.answerNickName"
                        >作者</span>
                    </p>
                  </div>
                  <span
                    class="iconfont icon-aixin"
                    v-if="!small.aixin2"
                    @click.stop="aixin2(1, index, index1)"
                    ><i>{{ small.praise == 0 ? "" : small.praise }}</i></span
                  >
                  <span
                    class="iconfont icon-aixin2 i"
                    v-if="small.aixin2"
                    @click.stop="aixin2(2, index, index1)"
                    ><i>{{ small.praise }}</i></span
                  >
                </div>
                <div class="items">
                  <p
                    class="ps"
                    v-if="details.operateUserName == small.answerNickName"
                  >
                    {{ small.content }}
                  </p>
                  <p
                    class="ps"
                    v-if="details.operateUserName != small.answerNickName"
                  >
                    回复 <span>@{{ small.atUserName }}</span
                    >: {{ small.content.split(":")[1] || small.content }}
                  </p>
                  <span class="times">{{small.createTime.split(" ")[0]}}</span>
                </div>
              </div>
            </div>
          </li>
        </ul>
      </div>
    </div>

```  
图片效果:

![](./assets/src/assets/imgs/../../../../src/assets/imgs/40.png)


## 设计方案
1. 使用vue2.0 vue-cli脚手架开发，实现单页面应用程序，页面切换时不刷新页面，且可以局部更新数据，轻量级项目，数据响应快，提高用户体验。
2. 请求api接口
3. vue-router3.0 路由懒加载，动态路由
4. axios挂载请求和响应支持promise API
5. vant2 按需引入，高质量组件
6. 瀑布流布局
   

## 使用到的技术
1. vue2.0
2. vue-router3.0
3. axios
4. vue-cli
5. vant
6. countTop数字滚动
   
## 效果截图-- [美鉴APP](https://note.youdao.com/s/DoudkJFL)
### 首页 
![](./assets/../src/assets/imgs/01首页.png)
### 发现页 
![](./assets/../src/assets/imgs/02发现页.png)
### 发现页瀑布流布局
![](./assets/../src/assets/imgs/03发现页瀑布流.png)
### 热门话题--小维鉴别 
![](./assets/../src/assets/imgs/04热门话题--小维鉴别.png)
### 我的口红日记 
![](./assets/../src/assets/imgs/05热门话题--我的口红日记.png)
### 我的护肤日常 
![](./assets/../src/assets/imgs/06热门话题--我的护肤日常--瀑布流布局.png)
### 帖子详情页 
![](./assets/../src/assets/imgs/07商品详情页01.png)
### 已点赞、评论、收藏 
![](./assets/../src/assets/imgs/08详情页--已点赞、评论、收藏.png)
### 发现页搜索 
![](./assets/../src/assets/imgs/09发现页搜索.png)
### 购物页 
![](./assets/../src/assets/imgs/10购物页.png)
### 购物页--搜索框-缓动-轮播 
![](./assets/../src/assets/imgs/11购物页--搜索框-缓动-轮播.png)
### 精选小样 
![](./assets/../src/assets/imgs/12精选小样.png)
### 限时秒杀 
![](./assets/../src/assets/imgs/13限时秒杀.png)
### 产品搜索 
![](./assets/../src/assets/imgs/15产品搜索.png)
### 闪电发货 
![](./assets/../src/assets/imgs/16闪电发货.png)
### 热门口红 
![](./assets/../src/assets/imgs/17热门口红.png)
### 产品列表 
![](./assets/../src/assets/imgs/18产品列表.png)
### 爆款洁面--tab联动 
![](./assets/../src/assets/imgs/19爆款洁面--tab联动.png)
### 分类页--口红 
![](./assets/../src/assets/imgs/20分类页--口红.png)
### 分类页-香水-销量降序 
![](./assets/../src/assets/imgs/21分类页-香水-销量降序.png)
### 分类页-气垫-价格降序 
![](./assets/../src/assets/imgs/22分类页-气垫-价格降序.png)
### 分类页-隔离-搜索结果 
![](./assets/../src/assets/imgs/23分类页-隔离-搜索结果.png)
### 全部分类 
![](./assets/../src/assets/imgs/24全部分类.png)
### 商品详情页 
![](./assets/../src/assets/imgs/25商品详情页.png)
### 添加购物车 
![](./assets/../src/assets/imgs/26添加购物车.png)
### 购物袋页面 
![](./assets/../src/assets/imgs/27购物袋页面.png)
### 确认订单页 
![](./assets/../src/assets/imgs/28确认订单页.png)
### 鉴别页 
![](./assets/../src/assets/imgs/29鉴别页.png)
### 付费鉴别 
![](./assets/../src/assets/imgs/30付费鉴别.png)
### 我的页 
![](./assets/../src/assets/imgs/31我的页.png)
### 待付款页-可点击去到确定订单 
![](./assets/../src/assets/imgs/32待付款页-可点击去到确定订单.png)
### 进行中页-可点击变为已完成 
![](./assets/../src/assets/imgs/33进行中页-可点击变为已完成.png)
### 已完成页 
![](./assets/../src/assets/imgs/34已完成页.png)
### 个人中心-可更换头像 
![](./assets/../src/assets/imgs/35个人中心-可更换头像.png)
### 更换头像后 
![](./assets/../src/assets/imgs/36更换头像后.png)
### 开通会员 
![](./assets/../src/assets/imgs/37开通会员.png)
### 我的鉴别结果 
![](./assets/../src/assets/imgs/38我的鉴别结果.png)
### 我的收藏 
![](./assets/../src/assets/imgs/39我的收藏.png)




