import request from "./http";


// 获取本地数据

// 推荐数据
export function getRecommendData(){
    return request({
        url:"./json/recommend.json",
        method:"get"
    })
}

// 热门话题
export function getHotData(){
    return request({
        url:"./json/hot.json",
        method:"get"
    })
}
// 最新数据
export function getNewData(){
    return request({
        url:"./json/new.json",
        method:"get"
    })
}
// 鉴别师
export function getDiscrnData(){
    return request({
        url:"./json/discrn.json",
        method:"get"
    })
}
// 小维鉴别最新数据
export function getXWNewData(){
    return request({
        url:"./json/xiaoweinew.json",
        method:"get"
    })
}
// 小维鉴别最热数据
export function getXWHotData(){
    return request({
        url:"./json/xiaoweihot.json",
        method:"get"
    })
}
// 我的口红日记最火数据
export function getMyHotData(){
    return request({
        url:"./json/myhot.json",
        method:"get"
    })
}
// 我的口红日记最新数据
export function getMyNewData(){
    return request({
        url:"./json/mynew.json",
        method:"get"
    })
}

// 我的护肤日常最新数据
export function getDailyNewData(){
    return request({
        url:"./json/dailynew.json",
        method:"get"
    })
}

// 我的护肤日常最火数据
export function getDailyHotData(){
    return request({
        url:"./json/dailyhot.json",
        method:"get"
    })
}

// 轮播图图片
export function getLunPicData(){
    return request({
        url:"./json/phone_lun.json",
        method:"get"
    })
}

// 导航nav
export function getAllNavData(){
    return request({
        url:"./json/all_nav.json",
        method:"get"
    })
}

// 导航nav全部详情
export function getNavCommData(){
    return request({
        url:"./json/nav_comm.json",
        method:"get"
    })
}

// 新人专区/小样
export function getWelfareData(){
    return request({
        url:"./json/welfare.json",
        method:"get"
    })
}

// 闪电发货/热门口红
export function getLightningData(){
    return request({
        url:"./json/lightning.json",
        method:"get"
    })
}
// 闪电发货全部
export function getLightAllData(){
    return request({
        url:"./json/lightning_move.json",
        method:"get"
    })
}
// 热门口红全部
export function getLipstickData(){
    return request({
        url:"./json/lipstick.json",
        method:"get"
    })
}


// 全部产品
export function getAllBuyData(){
    return request({
        url:"./json/all_buy.json",
        method:"get"
    })
}
// 轮播图数据
export function getLunBoData(){
    return request({
        url:"./json/lunbo.json",
        method:"get"
    })
}

// 小样
export function getSampleData(){
    return request({
        url:"./json/sample.json",
        method:"get"
    })
}

// 限时抢购
export function getSeckillData(){
    return request({
        url:"./json/seckill.json",
        method:"get"
    })
}

// 0元抽奖
export function getLuckData(){
    return request({
        url:"./json/luck.json",
        method:"get"
    })
}
// 搜索
// export function getSearchData(){
//     return request({
//         url:"./json/search.json",
//         method:"get"
//     })
// }
// 广告
export function getAdvertData(){
    return request({
        url:"./json/advert.json",
        method:"get"
    })
}
// 猜你喜欢
export function getLikeData(){
    return request({
        url:"./json/like.json",
        method:"get"
    })
}
// vip
export function getVipData(){
    return request({
        url:"./json/vip.json",
        method:"get"
    })
}

// 帖子详情信息
export function getAllDetailsData(){
    return request({
        url:"./json/alldetails.json",
        method:"get"
    })
}
// 最近购买
export function getPeopleData(){
    return request({
        url:"./json/people.json",
        method:"get"
    })
}
// 购买详情页
export function getShopData(){
    return request({
        url:"./json/shop.json",
        method:"get"
    })
}
// 鉴别页
export function getIdenData(){
    return request({
        url:"./json/iden.json",
        method:"get"
    })
}


