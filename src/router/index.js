import Vue from 'vue'
import VueRouter from 'vue-router'
import findView from "../views/findView"

Vue.use(VueRouter)
let beforeEnterFun = (to, from, next) => {
  let token = window.localStorage.getItem("token")
  // 判断是否登录
  if (!token) {
    next("/login")
    return
  } else {
    next()
  }
}
const routes = [

  // 定向到发现
  {
    path: '/',
    redirect: "/find"
  },
  // 发现
  {
    path: '/find',
    name: 'find',
    component: findView
  },
  // 购物
  {
    path: '/shopping',
    name: 'shopping',
    component: () => import('../views/shoppingView.vue'),
    meta: {//数据元
      keep: true,
    }
  },
  // 鉴别
  {
    path: '/identify',
    name: 'identify',
    component: () => import('../views/identifyView.vue'),
    meta: {//数据元
      keep: true,
    }
  },
  // 我的
  {
    path: '/my',
    name: 'my',
    component: () => import('../views/myView.vue')
  },
  // 登录
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/loginView.vue')
  },
  // 微信登录
  {
    path: '/wechat',
    name: 'wechat',
    component: () => import('../views/wechatView.vue')
  },
  // 个人信息
  {
    path: '/info',
    name: 'info',
    component: () => import('../views/infoView.vue'),
    beforeEnter: beforeEnterFun
  },
  // 修改名称
  {
    path: '/name',
    name: 'name',
    component: () => import('../views/nameView.vue'),
    beforeEnter: beforeEnterFun

  },
  // 热门话题跳转
  {
    path: '/allitem',
    name: 'allitem',
    component: () => import('../views/allitemView.vue')
  },
  // 设置
  {
    path: '/setup',
    name: 'setup',
    component: () => import('../views/setupView.vue')
  },

  // 空
  {
    path: '/null',
    name: 'null',
    component: () => import('../views/nullView.vue')
  },

  // 轮播图跳转
  {
    path: '/eleven',
    name: 'eleven',
    component: () => import('../views/elevenView.vue')
  },
  // 每日福利跳转
  {
    path: '/daily',
    name: 'daily',
    component: () => import('../views/dailyView.vue')
  },
  // 开奖详情
  {
    path: '/draw',
    name: 'draw',
    component: () => import('../views/drawView.vue'),

  },
  // 闪电发货/口红跳转
  {
    path: '/lightning',
    name: 'lightning',
    component: () => import('../views/lightningView.vue'),

  },
  //全部导航
  {
    path: '/allnav',
    name: 'allnav',
    component: () => import('../views/allnavView.vue'),

  },
  //导航详情
  {
    path: '/navigation',
    name: 'navigation',
    component: () => import('../views/navigationView.vue'),

  },
  //广告详情
  {
    path: '/advert',
    name: 'advert',
    component: () => import('../views/advertView.vue'),

  },
  //vip
  {
    path: '/vip',
    name: 'vip',
    component: () => import('../views/vipView.vue'),

  },
  //支付页面
  {
    path: '/pay',
    name: 'pay',
    component: () => import('../views/payView.vue'),
  },
  //帖子详情页
  {
    path: '/posts',
    name: 'posts',
    component: () => import('../views/postsView.vue'),
  },
  //我的收藏
  {
    path: '/collection',
    name: 'collection',
    component: () => import('../views/collectionView.vue'),
  },
  //购买详情
  {
    path: '/buy',
    name: 'buy',
    component: () => import('../views/buyView.vue'),
    
  },
  //最近购买
  {
    path: '/lately',
    name: 'lately',
    component: () => import('../views/latelyView.vue'),
  },

  //购物车
  {
    path: '/car',
    name: 'car',
    component: () => import('../views/carView.vue'),
  },
  //订单生成
  {
    path: '/order',
    name: 'order',
    component: () => import('../views/orderView.vue'),
  },
  //地址管理
  {
    path: '/address',
    name: 'address',
    component: () => import('../views/addressView.vue'),
  },
  //新建地址
  {
    path: '/ress',
    name: 'ress',
    component: () => import('../views/ressView.vue'),
  },
  //全部订单
  {
    path: '/allorder',
    name: 'allorder',
    component: () => import('../views/allorderView.vue'),
  },
  //搜索
  {
    path: '/search',
    name: 'search',
    component: () => import('../views/searchView.vue'),
  },
  //查询我的订单
  {
    path: '/query',
    name: 'query',
    component: () => import('../views/queryView.vue'),
  },
  
  //在线鉴别
  {
    path: '/discern',
    name: 'discern',
    component: () => import('../views/discernView.vue'),
  },
  
  //选择系列
  {
    path: '/choice',
    name: 'choice',
    component: () => import('../views/choiceView.vue'),
  },
  //发起鉴别
  {
    path: '/iden',
    name: 'iden',
    component: () => import('../views/idenView.vue'),
  },
  //我的鉴别
  {
    path: '/myiden',
    name: 'myiden',
    component: () => import('../views/myidenView.vue'),
  },
  





]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  //跳转了页面后显示在最上面
  scrollBehavior() {
    return { x: 0, y: 0 }
  }
})



export default router
