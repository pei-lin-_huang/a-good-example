import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false
import VueClipBoard from 'vue-clipboard2'
Vue.use(VueClipBoard)
import "./assets/font/iconfont.css"//引入字体图标
// 引入vant组件库
import { Button ,Tab, Tabs,Image as VanImage ,Loading,Lazyload ,PullRefresh ,List,Cell,Sticky ,Badge,Swipe, SwipeItem ,Toast,Checkbox, CheckboxGroup ,Uploader ,Slider,Popup ,Empty,Dialog,CountDown ,Overlay,RadioGroup,Radio,CellGroup , PasswordInput, NumberKeyboard,AddressEdit,Area ,IndexBar, IndexAnchor,Notify   } from 'vant';
Vue
.use(Button)
.use(Tab)
.use(Tabs)
.use(VanImage)
.use(Loading)
.use(Lazyload)
.use(PullRefresh)
.use(List)
.use(Cell)
.use(Sticky)
.use(Badge)
.use(Swipe)
.use(SwipeItem)
.use(Toast)
.use(Checkbox)
.use(CheckboxGroup)
.use(Uploader)
.use(Slider)
.use(Popup)
.use(Empty)
.use(Dialog)
.use(CountDown)
.use(Overlay)
.use(RadioGroup)
.use(Radio)
.use(CellGroup )
.use(PasswordInput)
.use(NumberKeyboard)
.use(AddressEdit)
.use(Area)
.use(IndexBar)
.use(IndexAnchor)
.use(Notify )

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
